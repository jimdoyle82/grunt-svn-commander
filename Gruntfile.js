/*
 * grunt-svn-commander
 * https://bitbucket.org/jimdoyle82/grunt-svn-commander/overview
 *
 * Copyright (c) 2013 James Doyle
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

      pkg: grunt.file.readJSON('package.json')

      ,svn_commander: {


        // set all "svn:ignore" props on all directories with a file called "svnignore.config"
        ignoreSetter: {
          options: {
            type: 'ignore'
            ,fileName: "svnignore.config"
          },
          rootDir: "test"
        }


        // set all "svn:externals" props on all directories with a file called "svnexternals.config"
        ,externalsSetter: {
          options: {
            type: 'externals'
            ,fileName: "svnexternals.config"
          },
          rootDir: "test"
        }


        // clear svn props recursively
        ,propsClearer: {
          options: {
            type: 'props-clear'
            ,clearRecursively: true
          },
          rootDir: "test"
        }


        // run any svn command, either as an array or a single string
        ,runCommands: {
          options: {
            type: 'command'
            ,cmd: ["svn info", "svn --version"]
            ,doneCallback: function( success ) {
              console.log( "SVN commands complete" );
            }
          }
        }

      } // end svn_commander
  });


  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  grunt.registerTask("default", [ "svn_commander:propsClearer", "svn_commander:ignoreSetter", "svn_commander:externalsSetter", "svn_commander:runCommands" ]);
};
