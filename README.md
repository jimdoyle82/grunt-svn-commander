# grunt-svn-commander

> Some useful SVN commands for your GruntJS builds.

## Getting Started
This plugin requires Grunt `0.4.x`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-svn-commander --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-svn-commander');
```

## The "svn_commander" task

### Overview
In your project's Gruntfile, add a section named `svn_commander` to the data object passed into `grunt.initConfig()`.

You must have SVN on the command-line for this task to work and you must have committed the target directory to your repo. Tested against svn 1.7.10.

```js
grunt.initConfig({
  svn_commander: {

    ignoreSetter: {
		options: {
			type: 'ignore'
			,doneCallback: function() {
				console.log( "all good" );
			}
		},
		rootDir: "test"
	}

  },
});
```

### Options

#### options.type
Type: `String`
Default value: `ignore`

A string value that is used to choose an svn command.

- `ignore` will look inside the specified `rootDir` path for a config file (default name `svnignore.config`) and use that to set the svn:ignore property. Accepts options `fileName` (String) and `doneCallback` (Function).

- `props-clear` will delete the specified svn properties on the specified `rootDir`. Accepts options `doneCallback` (Function), `clearRecursively` (Boolean) and `propType` (string).

- `command` allows you to easily run a custom SVN command. Accepts only the `cmd` (string) option. `rootDir` is irrelevant in this task.


#### options.fileName
Type: `String`
Default value: `ignore`

Specify a custom filename for the svn ignore config. Defaults to `svnignore.config`. Only for `type = ignore`.


#### options.doneCallback
Type: `Function`
Default value: `(empty function)`

Specify a callback for when the task is done. Defaults to an empty function. For `type` of `ignore` or `props-clear`.


#### options.clearRecursively
Type: `Booleam`
Default value: `true`

Specify fully recursive lookup for svn property clearage. Defaults to `true`. Only for `type = props-clear`.


#### options.propType
Type: `string`
Default value: `ignore`

Specifiy svn property to clear. Defaults to `ignore`. Only for `type = props-clear`.


#### options.cmd
Type: `string`
Default value: `(A sample 'echo' message)`

Specifiy a custom svn command. Defaults to a sample `echo` message. Only for `type = command`.




### Usage Examples

```js
svn_commander: {

  propsClearer: {
    options: {
      type: 'props-clear'
      ,propType: 'ignore'
      ,doneCallback: function() {
        console.log( "all props cleared" );
      }
    },
    rootDir: "test"
  }

  ,ignoreSetter: {
    options: {
      type: 'ignore'
      ,doneCallback: function() {
        console.log( "all ignore props set" );
      }
    },
    rootDir: "test"
  }

  ,runCommand: {
    options: {
      type: 'command'
      ,cmd: "echo 'running custom svn command\n'"
    }
  }
}
```



## Release History
- 0.1.8: Added support for arrays of svn clear props and defaulted to 'ignore' and 'externals'.
- 0.1.7: Fixed the grunt.spawn issue with 'svn commit' not working and added more specific logs (coloured).
- 0.1.6: Removed dot at the end of propset, as it was causing root to get unwanted props set.
- 0.1.5: Ignore "node_modules" folder when setting svnprops.
- 0.1.4: Added array of commands option and svn:externals option
- 0.1.3: Minor readme changes.
- 0.1.2: Passed tests on all tasks, but will need more real-world testing.
- 0.1.1: Made some more progress but still not functional.
- 0.1.0: Initial port over from grunt.jimd.utils. Still a work in progress.
