/*
 * grunt-svn-commander
 * https://bitbucket.org/jimdoyle82/grunt-svn-commander/overview
 *
 * Copyright (c) 2013 James Doyle
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

    var PLUGIN_NAME = "grunt-svn-commander"
        ,IGNORE = 'ignore'
        ,EXTERNALS = 'externals'
        ,PROPS_CLEAR = 'props-clear'
        ,COMMAND = 'command'
        ,exec = require('child_process').exec;

    var type = function ( test ) {
      return ( {} ).toString.call( test ).match( /\s([a-zA-Z]+)/ )[ 1 ].toLowerCase();
    }
    ,runSVNCommand = function( command, includeCallbackOnError, useExec, callback ) {

      
      if( !useExec ) {

        var  cmdArr   = command.split(' ')
            ,cmd      = cmdArr.shift()
            ,verbose  = !!(grunt.option("--verbose")) || !!(grunt.option("-v"));


        grunt.util.spawn({
           cmd: cmd
          ,args: cmdArr
        }, function( error, result, resultFallbackString, code ) {
          
          /**
            * Ignore if error is "svn: E200009: Could not add all targets because some targets are already versioned", as 
            * that just means we can't add an svn folder because it has already been added.
            */
          if( error && error.toString().indexOf( "E200009" ) != -1 ) error = null;

          if( error ) {
            if( verbose ) grunt.log.error( error, result, resultFallbackString, code );
            else      grunt.log.error( "ERROR: See message below or use --verbose option for more info. \n\n" + error + '\n\n Error code: ' + code + '\n\n' );
            
            if( includeCallbackOnError === true ) callback( false );
          } else {
            if( verbose ) grunt.log.ok( result );
            callback( true );
          }

        });

        return;
      }

      /**
        * Does the same thing, but not using Grunt. 
        * Grunt way above prefered, but with "svn commit" only this way would work for some reason.
        */
      exec(command, function (error, stdout) {


        var verbose = !!(grunt.option("--verbose")) || !!(grunt.option("-v"));
        if( verbose ) grunt.log.writeln( stdout );

        /**
          * Ignore if error is "svn: E200009: Could not add all targets because some targets are already versioned", as 
          * that just means we can't add an svn folder because it has already been added.
          */
        if( error && error.toString().indexOf( "E200009" ) != -1 ) error = null;

        if (error !== null) {
          
          if( verbose ) grunt.log.error( error, stdout );
          else          grunt.log.error( "ERROR: See message below or use --verbose option for more info. \n\n" + error + '\n\n' );

          if( includeCallbackOnError === true ) callback( false );
        } else {
          if( verbose ) grunt.log.ok( result );
          callback( true );
        }
      });

  }
  ,svnPropsClearer = function( propType, parentdir, recursive, callback ) {

    var propList = ( type(propType) === 'array' ? propType : [ propType ] )
        ,count = 0;

    var propClear = function() {

      runSVNCommand( "svn propdel svn:"+ propList[count] + " " + parentdir+"../ " + ( recursive ? " -R" : "" ) +" .", true, false, function() {
        grunt.log.success( "OK:  svn:"+ propList[count] +" removed on " + parentdir + ( recursive ? " recursively." : "." ) );

        count += 1;

        if( count === propList.length )  {
          // when list reaches end, finish up
          callback();
        } 
        else {
          propClear(); // loop through list
        }
      });
    }

    propClear( propList );
    /*runSVNCommand( "svn propdel svn:"+ propType + " " + parentdir+"../ " + ( recursive ? " -R" : "" ) +" .", true, false, function() {
      grunt.log.success( "OK:  svn:"+ propType +" removed on " + parentdir + ( recursive ? " recursively." : "." ) );

      callback();
    });*/
  }
  ,svnPropsSetter = function( propType, parentdir, configFileName, callback ) {

    /**
      * Place "configFileName" file in every directory that you want to set ignore/externals for. 
      * This file should contain a list of directory-level files or folders that you want to set ignore/externals on,
      * with line breaks seperating them.
      * Use globalling like so (for ignoring files only): *.jpg
      */
          
    // First gather a list of directories affected by searching for "configFileName" files.
    var dirList = [];
    grunt.file.recurse( parentdir, function( abspath, rootdir, subdir, filename ) {

      if( filename == configFileName && abspath.indexOf( "node_modules" ) == -1 ) {

        
        // root level "subdir" will be undefined
        // if( !subdir ) {
          // dirList.push( "./" );
          
        // } else {
        dirList.push( parentdir + (subdir||"") );
      }

    });

    if( dirList.length === 0 ) {

      grunt.log.warn("WARNING: No files called "+ configFileName + " found!");
      callback();
      return;
    }


    var count = 0;

    var propSet = function() {


      runSVNCommand( "svn propset svn:"+propType+" "+dirList[ count ]+" -F "+dirList[ count ]+"/"+configFileName, true, false, function() {
        grunt.log.success( "OK: svn:"+propType+" set on " + dirList[ count ] );

        count += 1;

        if( count === dirList.length )  {
          // when list reaches end, finish up
          callback();
        } 
        else {
          propSet(); // loop through list
        }
      });
    }

    // grunt.log.ok( "svn propset svn:"+propType+" "+dirList[ count ]+" -F "+dirList[ count ]+"/"+configFileName+" ." );
    propSet();
  }
  ,rootDirCheck = function( rootDir ) {

    if( rootDir && type( rootDir ) != "string" ) {
      grunt.log.error( "Error: " + PLUGIN_NAME + " 'rootDir' must be set and must be a string." );
      return false;
    }

    return true;
  }
  ,runArrayOfSVNCommands = function( cmdArr, incr, svnDone ) {

    if( incr === cmdArr.length ) {
      svnDone();
      return;
    }

    var useExec = true; // some cases, like 'svn commit' didn't work with Grunt spawn for some reason

    runSVNCommand( cmdArr[ incr ], true, useExec, function() {

      grunt.log.writeln( "" );
      grunt.log.success( "OK: SVN command complete "+ (incr+1) +" of "+ cmdArr.length );
      grunt.log.writeln( "" );
      runArrayOfSVNCommands( cmdArr, incr+1, svnDone );
    });
  }


  grunt.registerMultiTask('svn_commander', 'Some useful SVN commands for your GruntJS builds.', function() {
    
    // If this.async() isn't called none of the callbacks work on the svn commands and other commands won't work after this.
    var doneAsync = this.async();
    
    var options = this.options({        // Merge task-specific and/or target-specific options with these defaults.
           type: IGNORE                 // type of task you want to run. 'ignore', 'externals', 'props-clear' or 'command'
          ,fileName: 'svnprops.config' // name of file to look for in each directory, which will hold ignore or externals info
          ,clearRecursively: true       // for props-clear, whether or not to recursively clear or just do top directory
          ,doneCallback: function() {
            grunt.log.write( 'doneCallback triggered' );
          }
          ,propType: [ IGNORE, EXTERNALS ]   // type of svn property that you want to affect (eg 'ignore' equates to 'svn:ignore')
          ,cmd: "echo 'Please override options.cmd with your own svn command.\n'"
        })
        ,data = this.data
        ,rootDir;


    // Each exec command requires doneAsync to be called, so that it can tell the next task to execute.
    var svnDone = function() {
      options.doneCallback();
      doneAsync();
    }


    if( options.type == IGNORE || options.type == EXTERNALS || options.type == PROPS_CLEAR ) {

      if( type(data) === 'string' ) rootDir = data;
      else                          rootDir = data.rootDir;

      // check rootDir is a string
      if(  !rootDirCheck( rootDir )  ) return;


      var lastRootChar = rootDir.charAt( rootDir.length-1 );

      // Make sure the rootDir always ends in a '/'
      if( lastRootChar != '/' ) rootDir += '/';
    }


    if( options.type == PROPS_CLEAR ) {
      if( type(options.propType) !== 'string' && type(options.propType) !== 'array' )
        grunt.log.error( "ERROR: Option " + PROPS_CLEAR + " must have a 'propType' of either a string or an array of strings." );
    }

    switch( options.type ) {


      case IGNORE:
      case EXTERNALS:
        svnPropsSetter( options.type, rootDir, options.fileName, svnDone );
        break;


      case PROPS_CLEAR:
        svnPropsClearer( options.propType, rootDir, options.clearRecursively, svnDone );
        break;


      case COMMAND:

        var useExec = true; // some cases, like 'svn commit' didn't work with Grunt spawn for some reason

        if( type(options.cmd) === "array" ) runArrayOfSVNCommands( options.cmd, 0, svnDone );
        else                                runSVNCommand( options.cmd, true, useExec, svnDone);
        break;

    }
    
  });

};
